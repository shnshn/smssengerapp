package by.smsender.helper;

public class ConstantsPreferenceKeys {

	public static String KEY_CURRENT_EMAIL = "current_email";
	public static String KEY_CURRENT_PASSWORD = "current_password";
	public static String KEY_CURRENT_TOKEN = "current_token";
	public static String KEY_ALARM_IS_ACTIVE = "active_alarm";
	public static String KEY_ID = "id";
	public static String KEY_INTERVAL = "interval";
	public static String KEY_CURRENT_URL = "current_url";
	public static String KEY_CURRENT_HOUR = "current_hour";
	public static String KEY_CURRENT_MINUTE = "current_minute";
	public static String KEY_CURRENT_SECONDS = "current_seconds";
	public static String KEY_WIFI = "wifi_only";
	public static String KEY_USER_STATUS = "user_status";

}
