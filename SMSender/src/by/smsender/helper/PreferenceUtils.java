package by.smsender.helper;

import by.smsender.model.Constants;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

public class PreferenceUtils {

	public static void setCurrentPhone(Context context, String phone) {
		Editor pEditor = PreferenceManager.getDefaultSharedPreferences(context)
				.edit();
		pEditor.putString("number", phone);
		pEditor.commit();
	}

	public static String getCurrentPhone(Context context) {
		SharedPreferences defaultSharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(context);
		return defaultSharedPreferences.getString("number", "");
	}

	public static void setCurrentUserEmail(Context context, String email) {
		Editor pEditor = PreferenceManager.getDefaultSharedPreferences(context)
				.edit();
		pEditor.putString(ConstantsPreferenceKeys.KEY_CURRENT_EMAIL, email);
		pEditor.commit();
	}

	public static String getCurrentUserEmail(Context context) {
		SharedPreferences defaultSharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(context);
		return defaultSharedPreferences.getString(
				ConstantsPreferenceKeys.KEY_CURRENT_EMAIL, "");
	}

	public static void setCurrentUserPassword(Context context, String password) {
		Editor pEditor = PreferenceManager.getDefaultSharedPreferences(context)
				.edit();
		pEditor.putString(ConstantsPreferenceKeys.KEY_CURRENT_PASSWORD,
				password);
		pEditor.commit();
	}

	public static String getCurrentUserPassword(Context context) {
		SharedPreferences defaultSharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(context);
		return defaultSharedPreferences.getString(
				ConstantsPreferenceKeys.KEY_CURRENT_PASSWORD, "");
	}

	public static void setCurrentUserToken(Context context, String token) {
		Editor pEditor = PreferenceManager.getDefaultSharedPreferences(context)
				.edit();
		pEditor.putString(ConstantsPreferenceKeys.KEY_CURRENT_TOKEN, token);
		pEditor.commit();
	}

	public static String getCurrentUserToken(Context context) {
		SharedPreferences defaultSharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(context);
		return defaultSharedPreferences.getString(
				ConstantsPreferenceKeys.KEY_CURRENT_TOKEN, "");
	}

	public static void setStatusAlarm(Context context, boolean flag) {
		Editor pEditor = PreferenceManager.getDefaultSharedPreferences(context)
				.edit();
		pEditor.putBoolean(ConstantsPreferenceKeys.KEY_ALARM_IS_ACTIVE, flag);
		pEditor.commit();
	}

	public static boolean isActiveAlarm(Context context) {
		SharedPreferences defaultSharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(context);
		return defaultSharedPreferences.getBoolean(
				ConstantsPreferenceKeys.KEY_ALARM_IS_ACTIVE, false);
	}

	public static void setId(Context context, int id) {
		Editor pEditor = PreferenceManager.getDefaultSharedPreferences(context)
				.edit();
		pEditor.putInt(ConstantsPreferenceKeys.KEY_ID, id);
		pEditor.commit();
	}

	public static int getId(Context context) {
		SharedPreferences defaultSharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(context);
		return defaultSharedPreferences.getInt(ConstantsPreferenceKeys.KEY_ID,
				0);
	}

	public static void setInterval(Context context, long timeMIN) {
		Editor pEditor = PreferenceManager.getDefaultSharedPreferences(context)
				.edit();
		pEditor.putLong(ConstantsPreferenceKeys.KEY_INTERVAL, timeMIN);
		pEditor.commit();
	}

	public static long getInterval(Context context) {
		SharedPreferences defaultSharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(context);
		return defaultSharedPreferences.getLong(
				ConstantsPreferenceKeys.KEY_INTERVAL, 30000);
	}

	public static void setInt(Context context, String key, int val) {
		Editor pEditor = PreferenceManager.getDefaultSharedPreferences(context)
				.edit();
		pEditor.putInt(key, val);
		pEditor.commit();
	}

	public static int getInt(Context context, String key) {
		SharedPreferences defaultSharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(context);
		int def = 0;
		if (key.equals(ConstantsPreferenceKeys.KEY_CURRENT_SECONDS))
			def = 30;
		return defaultSharedPreferences.getInt(key, def);
	}

	public static void setCurrentUrl(Context context, String url) {
		Editor pEditor = PreferenceManager.getDefaultSharedPreferences(context)
				.edit();
		pEditor.putString(ConstantsPreferenceKeys.KEY_CURRENT_URL, url);
		pEditor.commit();
	}

	public static String getCurrentURL(Context context) {
		SharedPreferences defaultSharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(context);
		return defaultSharedPreferences.getString(
				ConstantsPreferenceKeys.KEY_CURRENT_URL, Constants.URL_DEFAULT);
	}

	public static void setWifiFlaf(Context context, boolean flag) {
		Editor pEditor = PreferenceManager.getDefaultSharedPreferences(context)
				.edit();
		pEditor.putBoolean(ConstantsPreferenceKeys.KEY_WIFI, flag);
		pEditor.commit();
	}

	public static boolean isOnlyWifi(Context context) {
		SharedPreferences defaultSharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(context);
		return defaultSharedPreferences.getBoolean(
				ConstantsPreferenceKeys.KEY_WIFI, false);
	}

	public static void setUserGoldStatus(Context context, boolean flag) {
		Editor pEditor = PreferenceManager.getDefaultSharedPreferences(context)
				.edit();
		pEditor.putBoolean(ConstantsPreferenceKeys.KEY_USER_STATUS, flag);
		pEditor.commit();
	}

	public static boolean isUserGold(Context context) {
		SharedPreferences defaultSharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(context);
		return defaultSharedPreferences.getBoolean(
				ConstantsPreferenceKeys.KEY_USER_STATUS, false);
	}

}
