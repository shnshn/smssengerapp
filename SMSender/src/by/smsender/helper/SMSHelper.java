package by.smsender.helper;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Calendar;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.telephony.SmsManager;
import android.util.Log;
import by.smsender.db.DatabaseMgr;
import by.smsender.model.SMSTask;
import by.smsender.web.WebUtils;

public class SMSHelper {

	// ---sends an SMS message to another device---
	public static void sendSMS(SMSTask task, final Context context) {
		String SENT = "SMS_SENT";
		String DELIVERED = "SMS_DELIVERED";

		int id = PreferenceUtils.getId(context);
		PreferenceUtils.setId(context, id + 1);
		Log.d("SMS_STATUS", "ID" + id);

		Intent intent_data_sent = new Intent(SENT + id);
		Intent intent_data_delivered = new Intent(DELIVERED + id);
		intent_data_sent.putExtra("id", task.id);
		intent_data_delivered.putExtra("id", task.id);
		intent_data_sent.putExtra("number", task.num);
		intent_data_delivered.putExtra("number", task.num);

		PendingIntent sentPI = PendingIntent.getBroadcast(context, id,
				intent_data_sent, PendingIntent.FLAG_CANCEL_CURRENT);

		PendingIntent deliveredPI = PendingIntent.getBroadcast(context, id,
				intent_data_delivered, PendingIntent.FLAG_CANCEL_CURRENT);

		// ---when the SMS has been sent---
		context.registerReceiver(new BroadcastReceiver() {
			@Override
			public void onReceive(Context arg0, Intent arg1) {
				String number = arg1.getExtras().getString("number");
				int id = arg1.getExtras().getInt("id");
				SMSTask task = new SMSTask();
				task.num = number;
				task.id = id;
				String message = "unknown";
				switch (getResultCode()) {
				case Activity.RESULT_OK:
					message = "SMS sent";
					break;
				case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
					message = "Generic failure";
					break;
				case SmsManager.RESULT_ERROR_NO_SERVICE:
					message = "No services";
					break;
				case SmsManager.RESULT_ERROR_NULL_PDU:
					message = "Null PDU";
					break;
				case SmsManager.RESULT_ERROR_RADIO_OFF:
					message = "Radio off";
					break;
				}
				Log.d("SMS_STATUS", "number = " + number + "; status = "
						+ message + "  " + this.getResultCode());
				Calendar c = Calendar.getInstance();
				int seconds = c.get(Calendar.SECOND);
				task.msg = message + "--" + c.get(Calendar.HOUR) + ":"
						+ c.get(Calendar.MINUTE) + ":" + seconds;

				DatabaseMgr.getInstance(context).putSMSTask(task,
						DatabaseMgr.FLAG_NEW_STATUSES);
				DatabaseMgr.getInstance(context).putLog(task.toString());
				context.unregisterReceiver(this);
			}
		}, new IntentFilter(SENT + id));
		// context.unregisterReceiver(receiver)

		// ---when the SMS has been delivered---
		context.registerReceiver(new BroadcastReceiver() {
			@Override
			public void onReceive(Context arg0, Intent arg1) {

				String number = arg1.getExtras().getString("number");
				int id = arg1.getExtras().getInt("id");
				SMSTask task = new SMSTask();
				task.id = id;
				task.num = number;
				Calendar c = Calendar.getInstance();
				int seconds = c.get(Calendar.SECOND);
				task.msg = "sms_delivered" + "--" + c.get(Calendar.HOUR) + ":"
						+ c.get(Calendar.MINUTE) + ":" + seconds;
				Log.d("SMS_STATUS", "СМС ДОСТАВЛЕНО" + this.getResultCode());
				DatabaseMgr.getInstance(context).updateNewTasks(task.id,
						task.msg);
				DatabaseMgr.getInstance(context).putLog(task.toString());
				context.unregisterReceiver(this);
			}
		}, new IntentFilter(DELIVERED + id));

		SmsManager sms = SmsManager.getDefault();
		try {
			task.msg = URLDecoder.decode(task.msg, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		sms.sendTextMessage(task.num, null, task.msg, sentPI, deliveredPI);
	}

}
