package by.smsender.asyncTasks;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;
import by.smsender.helper.SMSHelper;
import by.smsender.model.SMSTask;

public class SMSIntentService extends IntentService {

	final String LOG_TAG = "myLogs";

	public SMSIntentService() {
		super("myname");
	}

	public void onCreate() {
		super.onCreate();
		Log.d(LOG_TAG, "onCreate");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		SMSTask task = (SMSTask) intent.getSerializableExtra("sms_extra");
		SMSHelper.sendSMS(task, getApplicationContext());
	}

	public void onDestroy() {
		super.onDestroy();
		Log.d(LOG_TAG, "onDestroy");
	}
}