package by.smsender.asyncTasks;

import java.util.List;

import android.content.Context;
import android.os.AsyncTask;
import by.smsender.db.DatabaseMgr;
import by.smsender.helper.PreferenceUtils;
import by.smsender.model.SMSTask;
import by.smsender.web.SMSHttpClient;

public class AsynkUpdateTaskStatus extends AsyncTask<Void, Void, Void> {

	Context context;

	public AsynkUpdateTaskStatus(Context context) {
		this.context = context;
	}

	@Override
	protected Void doInBackground(Void... params) {
		List<SMSTask> tasks = DatabaseMgr.getInstance(context).getAllTasks(
				DatabaseMgr.FLAG_NEW_STATUSES);
		if (tasks != null && tasks.size() != 0) {
			SMSHttpClient client = new SMSHttpClient(context);
			client.updateTaskStatus(
					PreferenceUtils.getCurrentUserEmail(context),
					PreferenceUtils.getCurrentUserToken(context), tasks);
			DatabaseMgr.getInstance(context).deleteNewTasks();
		}

		return null;
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);

	}

}
