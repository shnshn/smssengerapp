package by.smsender.asyncTasks;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import by.smsender.helper.PreferenceUtils;
import by.smsender.helper.SMSHelper;
import by.smsender.model.SMSTask;
import by.smsender.model.json.JSONTask;
import by.smsender.web.SMSHttpClient;

public class AsyncGetTaskSendSMS extends AsyncTask<Void, Void, JSONTask> {
	Context context;

	public AsyncGetTaskSendSMS(Context context) {
		this.context = context;
	}

	@Override
	protected JSONTask doInBackground(Void... params) {
		SMSHttpClient httpClient = new SMSHttpClient(context);

		return httpClient.getTasks(
				PreferenceUtils.getCurrentUserEmail(context),
				PreferenceUtils.getCurrentUserToken(context), "34");
	}

	@Override
	protected void onPostExecute(JSONTask result) {
		super.onPostExecute(result);
		// AsyncSendSMS asyncTask = new AsyncSendSMS(context, result);
		// asyncTask.execute();

		if (result != null && result.res && result.nums != null
				&& result.nums.size() != 0) {
			for (SMSTask item : result.nums) {
				Intent smsTaskIntent = new Intent(context,
						SMSIntentService.class);
				// smsTaskIntent.addFlags(Intent.)
				smsTaskIntent.putExtra("sms_extra", item);
				context.startService(smsTaskIntent);

			}
		}
	}

}
