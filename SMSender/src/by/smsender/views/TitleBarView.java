package by.smsender.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import by.smsender.R;

public class TitleBarView extends RelativeLayout {

	public TitleBarView(Context context) {
		super(context);
		initLayout(context);
	}

	public TitleBarView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initLayout(context);
	}

	public TitleBarView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		initLayout(context);
	}

	private void initLayout(Context context) {
		LayoutInflater.from(context).inflate(R.layout.title_bar_view, this,
				true);
	}

	public FrameLayout getBackButton() {
		return (FrameLayout) findViewById(R.id.but_back_fragment);
	}

	public Button getSearchButton() {
		return (Button) findViewById(R.id.title_bar_search_btn);
	}
	
	public View getIcon() {
		return  findViewById(R.id.img_icon);
	}

	public FrameLayout getBackActivityButton() {
		return (FrameLayout) findViewById(R.id.but_back_activity);
	}

	public void hideAllBackButton() {
		findViewById(R.id.lay_title_back_but).setVisibility(View.GONE);
		findViewById(R.id.lay_titile_activity_but).setVisibility(View.GONE);
		findViewById(R.id.but_back_fragment).setVisibility(View.GONE);
		findViewById(R.id.but_back_activity).setVisibility(View.GONE);
	}

	public void setBackButtonHidden(boolean hidden) {
		findViewById(R.id.lay_title_back_but).setVisibility(
				hidden ? View.GONE : View.VISIBLE);
		findViewById(R.id.lay_titile_activity_but).setVisibility(
				hidden ? View.VISIBLE : View.GONE);
		findViewById(R.id.but_back_fragment).setVisibility(
				hidden ? View.GONE : View.VISIBLE);
		findViewById(R.id.but_back_activity).setVisibility(
				hidden ? View.VISIBLE : View.GONE);
	}

	public void setTitle(String title) {
		((TextView) findViewById(R.id.title_bar_title)).setText(title);
	}

	public void setSearchButtonHidden(boolean hidden) {
		findViewById(R.id.title_bar_search_btn).setVisibility(
				hidden ? View.GONE : View.VISIBLE);
	}

}
