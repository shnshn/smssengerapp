package by.smsender;

import java.util.List;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import by.smsender.db.DatabaseMgr;
import by.smsender.fragments.ParentFragment;
import by.smsender.views.TitleBarView;

@SuppressLint("NewApi")
public class LogActivity extends ParentActivity {

	Button but_clear;
	ListView list;
	ArrayAdapter<String> adapter;
	View viewNoData;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_log);

		mTitleBar = (TitleBarView) findViewById(R.id.top_title_bar);
		mTitleBar.setBackButtonHidden(true);
		mTitleBar.setSearchButtonHidden(false);

		OnClickListener clickListener = new OnClickListener() {

			@Override
			public void onClick(View v) {
				onBackPressed();

			}
		};
		mTitleBar.getBackActivityButton().setOnClickListener(clickListener);
		mTitleBar.getIcon().setOnClickListener(clickListener);

		mTitleBar.getSearchButton().setBackground(
				getResources().getDrawable(R.drawable.clear));

		int wight = (int) TypedValue.applyDimension(
				TypedValue.COMPLEX_UNIT_DIP, 60, getResources()
						.getDisplayMetrics());

		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
				wight, RelativeLayout.LayoutParams.WRAP_CONTENT);
		params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
		params.addRule(RelativeLayout.CENTER_VERTICAL, RelativeLayout.TRUE);
		mTitleBar.getSearchButton().setLayoutParams(params);

		mTitleBar.getBackActivityButton().setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View v) {
						onBackPressed();
					}
				});
		mTitleBar.setTitle("Log");
		viewNoData = (View) findViewById(R.id.view_no_data);

		List<String> data = DatabaseMgr.getInstance(getApplicationContext())
				.getAllLog();
		list = (ListView) findViewById(R.id.list_log);
		if (data == null || data.size() == 0) {
			showNoData(true);
		} else {
			adapter = new ArrayAdapter<String>(this, R.layout.text_item_log);
			adapter.addAll(data);
			list.setAdapter(adapter);
			showNoData(false);
		}

	}

	public void showNoData(boolean flag) {
		if (flag) {
			list.setVisibility(View.GONE);
			viewNoData.setVisibility(View.VISIBLE);
		} else {
			list.setVisibility(View.VISIBLE);
			viewNoData.setVisibility(View.GONE);
		}
	}

	@Override
	public void onBackPressed() {

		// int count = getFragmentManager().getBackStackEntryCount();
		ParentFragment pFrag = (ParentFragment) getSupportFragmentManager()
				.findFragmentByTag("empty");
		if (mTitleBar.getBackButton().getVisibility() == View.GONE) {
			super.onBackPressed();
		} else if (mTitleBar.getBackButton().getVisibility() == View.VISIBLE) {
			pFrag.popChildFragment();
		}

	}

}
