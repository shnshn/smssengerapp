package by.smsender.db;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import by.smsender.model.SMSTask;

public class DatabaseMgr {

	private static final String TAG = "DatabaseMgr";

	private static DatabaseMgr instance = null;

	private SQLiteDatabase db = null;

	public static synchronized DatabaseMgr getInstance(Context context) {
		if (instance == null)
			instance = new DatabaseMgr(context);
		return instance;
	}

	private DatabaseMgr(Context context) {
		connect(context);
	}

	public void connect(Context context) {
		if (db != null)
			return;
		db = (new SMSTaskDbHelper(context)).getWritableDatabase();
	}

	public static final int FLAG_NEW_STATUSES = 0;
	public static final int FLAG_SENT_STATUSES = 1;

	public List<SMSTask> getAllTasks(int flag) {

		// db.beginTransaction();
		List<SMSTask> tasks = new ArrayList<SMSTask>();
		// try {
		String queryTest = "Select * FROM ";

		switch (flag) {
		case FLAG_NEW_STATUSES:
			queryTest += ConstantsDB.TABLE_NEW_TASK_STATUS;
			break;
		case FLAG_SENT_STATUSES:
			queryTest += ConstantsDB.TABLE_ALL_SENT_TASK_STATUS;
			break;

		default:
			break;
		}

		Cursor cursor = db.rawQuery(queryTest, new String[] {});

		int columnId = cursor.getColumnIndex(ConstantsDB.COLUMN_TASK_ID);
		int columnNumber = cursor
				.getColumnIndex(ConstantsDB.COLUMN_TASK_NUMBER);
		int columnStatus = cursor
				.getColumnIndex(ConstantsDB.COLUMN_TASK_STATUS);

		SMSTask task;
		if (cursor.getCount() != 0) {
			cursor.moveToFirst();

			do {
				task = new SMSTask();
				task.id = cursor.getInt(columnId);
				task.num = cursor.getString(columnNumber);
				task.msg = cursor.getString(columnStatus);

				tasks.add(task);

			} while (cursor.moveToNext());

			cursor.close();
		} else {
			tasks = null;
		}
		// db.setTransactionSuccessful();
		// } catch (Exception e) {
		//
		// e.printStackTrace();
		// Log.d("DB", "ERROR READ DB");
		// return null;
		// } finally {
		// db.endTransaction();
		// }
		//
		// db.close();
		return tasks;
	}

	public void putSMSTasks(List<SMSTask> tasks, int flag_table) {
		// db.beginTransaction();
		// try {
		for (SMSTask graphUser : tasks) {
			putSMSTask(graphUser, flag_table);
		}
		// db.setTransactionSuccessful();
		// } catch (Exception e) {
		//
		// e.printStackTrace();
		// Log.d("DB", "ERROR READ DB");
		// } finally {
		// db.endTransaction();
		// }
		//
		// db.close();
	}

	public void deleteNewTasks() {

		db.delete(ConstantsDB.TABLE_NEW_TASK_STATUS, null, null);
	}

	public void updateNewTasks(int id, String status) {
		ContentValues cv = new ContentValues();
		cv.put(ConstantsDB.COLUMN_TASK_STATUS, status);

		db.update(ConstantsDB.TABLE_NEW_TASK_STATUS, cv,
				ConstantsDB.COLUMN_TASK_ID + " = ?", new String[] { "" + id });

	}

	public void putSMSTask(SMSTask task, int flag_table) {
		if (task == null)
			return;
		ContentValues cv = new ContentValues();
		cv.put(ConstantsDB.COLUMN_TASK_ID, task.id);
		cv.put(ConstantsDB.COLUMN_TASK_NUMBER, task.num);
		cv.put(ConstantsDB.COLUMN_TASK_STATUS, task.msg);
		String table = "";
		switch (flag_table) {
		case FLAG_NEW_STATUSES:
			table = ConstantsDB.TABLE_NEW_TASK_STATUS;
			break;
		case FLAG_SENT_STATUSES:
			table = ConstantsDB.TABLE_ALL_SENT_TASK_STATUS;
			break;

		default:
			break;
		}
		db.insert(table, null, cv);
	}

	public List<String> getAllLog() {

		List<String> tasks = new ArrayList<String>();
		String queryTest = "Select * FROM " + ConstantsDB.TABLE_LOG;

		Cursor cursor = db.rawQuery(queryTest, new String[] {});

		int columnText = cursor.getColumnIndex(ConstantsDB.COLUMN_LOG_TEXT);

		String task;
		if (cursor.getCount() != 0) {
			cursor.moveToFirst();

			do {
				task = new String();
				task = cursor.getString(columnText);
				tasks.add(task);

			} while (cursor.moveToNext());

			cursor.close();
		} else {
			tasks = null;
		}

		return tasks;
	}

	public void deleteAllLog() {

		db.delete(ConstantsDB.TABLE_LOG, null, null);
	}

	public void putLog(String task) {
		if (task == null)
			return;
		ContentValues cv = new ContentValues();
		cv.put(ConstantsDB.COLUMN_LOG_TEXT, task);
		String table = ConstantsDB.TABLE_LOG;
		db.insert(table, null, cv);
	}

}
