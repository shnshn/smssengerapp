package by.smsender.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SMSTaskDbHelper extends SQLiteOpenHelper {

	private static final String DATABASE_NAME = "sms.db";
	private static final int DATABASE_VERSION = 1;

	public SMSTaskDbHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {

		String CREATE_NEW_STATUS_TABLE = "CREATE TABLE "
				+ ConstantsDB.TABLE_NEW_TASK_STATUS + "("
				+ ConstantsDB.COLUMN_ID + " INTEGER PRIMARY KEY,"
				+ ConstantsDB.COLUMN_TASK_ID + " INTEGER,"
				+ ConstantsDB.COLUMN_TASK_NUMBER + " TEXT,"
				+ ConstantsDB.COLUMN_TASK_STATUS + " TEXT" + ")";

		String CREATE_SENT_STATUS = "CREATE TABLE "
				+ ConstantsDB.TABLE_ALL_SENT_TASK_STATUS + "("
				+ ConstantsDB.COLUMN_ID + " INTEGER PRIMARY KEY,"
				+ ConstantsDB.COLUMN_TASK_ID + " INTEGER,"
				+ ConstantsDB.COLUMN_TASK_NUMBER + " TEXT,"
				+ ConstantsDB.COLUMN_TASK_STATUS + " TEXT" + ")";

		String CREATE_LOG = "CREATE TABLE " + ConstantsDB.TABLE_LOG + "("
				+ ConstantsDB.COLUMN_ID + " INTEGER PRIMARY KEY,"
				+ ConstantsDB.COLUMN_LOG_TEXT + " TEXT" + ")";

		db.execSQL(CREATE_NEW_STATUS_TABLE);
		db.execSQL(CREATE_SENT_STATUS);
		db.execSQL(CREATE_LOG);

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldV, int newV) {
		if (newV > oldV) {
			db.execSQL("drop table if exists image_cache");
			onCreate(db);
		}
	}
}
