package by.smsender.db;

public class ConstantsDB {

	public static final String TABLE_NEW_TASK_STATUS = "new_task_status_table";
	public static final String TABLE_ALL_SENT_TASK_STATUS = "sent_task_status_table";
	public static final String TABLE_LOG = "log_table";

	public static final String COLUMN_ID = "_id";

	public static final String COLUMN_TASK_ID = "task_id";
	public static final String COLUMN_TASK_NUMBER = "task_number";
	public static final String COLUMN_TASK_STATUS = "task_status";
	public static final String COLUMN_LOG_TEXT = "log_text";

}
