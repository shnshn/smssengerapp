package by.smsender;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import by.smsender.fragments.FragmentContainer;
import by.smsender.fragments.ParentFragment;
import by.smsender.helper.PreferenceUtils;
import by.smsender.views.TitleBarView;

public class RegisterActivity extends ParentActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		mTitleBar = (TitleBarView) findViewById(R.id.top_title_bar);
		mTitleBar.setSearchButtonHidden(true);
		OnClickListener clickListener = new OnClickListener() {

			@Override
			public void onClick(View v) {
				onBackPressed();

			}
		};
		mTitleBar.getBackActivityButton().setOnClickListener(clickListener);
		mTitleBar.getIcon().setOnClickListener(clickListener);
		
		if (!PreferenceUtils.getCurrentUserEmail(getApplicationContext())
				.isEmpty()
				&& !PreferenceUtils
						.getCurrentUserToken(getApplicationContext()).isEmpty()) {

			Intent intent = new Intent(this, MainActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
			startActivity(intent);
			this.finish();

		} else {
			replaceFragment(new FragmentContainer(), false);
		}

//		SharedPreferences prefs = getSharedPreferences("ShortCutPrefs",
//				MODE_PRIVATE);
//		Log.d("SETUP", "SETUP");
//		if (!prefs.getBoolean("isFirstTime", false)) {
//			addShortcut(this);
//			SharedPreferences.Editor editor = prefs.edit();
//			editor.putBoolean("isFirstTime", true);
//			editor.commit();
//		}

	}

	private void addShortcut(Context context) {
		// Adding shortcut for MainActivity on Home screen
		Intent shortcutIntent = new Intent(context.getApplicationContext(),
				MainActivity.class);

		shortcutIntent.setAction(Intent.ACTION_MAIN);

		Intent addIntent = new Intent();
		addIntent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
		addIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, context.getResources()
				.getString(R.string.app_name));
		addIntent
				.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE,
						Intent.ShortcutIconResource.fromContext(
								context.getApplicationContext(),
								R.drawable.ic_launcher));

		addIntent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
		context.getApplicationContext().sendBroadcast(addIntent);
	}

	@Override
	public void onBackPressed() {

		// int count = getFragmentManager().getBackStackEntryCount();
		ParentFragment pFrag = (ParentFragment) getSupportFragmentManager()
				.findFragmentByTag("empty");
		if (mTitleBar.getBackButton().getVisibility() == View.GONE) {

			moveTaskToBack(true);
			super.onBackPressed();
		} else if (mTitleBar.getBackButton().getVisibility() == View.VISIBLE) {
			pFrag.popChildFragment();
		}

	}
}
