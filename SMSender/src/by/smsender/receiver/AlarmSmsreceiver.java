package by.smsender.receiver;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.PowerManager;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;
import by.smsender.asyncTasks.AsyncGetTaskSendSMS;
import by.smsender.asyncTasks.AsynkUpdateTaskStatus;
import by.smsender.db.DatabaseMgr;
import by.smsender.helper.PreferenceUtils;
import by.smsender.helper.SMSHelper;
import by.smsender.model.RegisttLoginModel;
import by.smsender.model.SMSTask;
import by.smsender.model.json.JSONTask;
import by.smsender.web.SMSHttpClient;
import by.smsender.web.WebUtils;

@SuppressLint("Wakelock")
public class AlarmSmsReceiver extends BroadcastReceiver {
	public int ID_ALARM = 123;

	@Override
	public void onReceive(Context context, Intent intent) {
		PowerManager pm = (PowerManager) context
				.getSystemService(Context.POWER_SERVICE);
		PowerManager.WakeLock wl = pm.newWakeLock(
				PowerManager.PARTIAL_WAKE_LOCK, "");
		wl.acquire();
		Log.d("ALARM_SMS", "Работаю я, маша я =)");

		if (WebUtils.isOnline(context)) {

			if (PreferenceUtils.isOnlyWifi(context) && WebUtils.isWifi(context)) {
				doTask(context);
			} else if (!PreferenceUtils.isOnlyWifi(context)) {
				doTask(context);
			}

		} else {
			DatabaseMgr.getInstance(context).putLog(
					"ERROR. No internet connection for geting tasks");
		}

		wl.release();
	}

	public void doTask(Context context) {
		AsynkUpdateTaskStatus syStatus = new AsynkUpdateTaskStatus(context);
		syStatus.execute();

		AsyncGetTaskSendSMS sync = new AsyncGetTaskSendSMS(context);
		sync.execute();
	}

	public void SetAlarm(Context context, long period) {
		AlarmManager am = (AlarmManager) context
				.getSystemService(Context.ALARM_SERVICE);
		Intent intent = new Intent(context, AlarmSmsReceiver.class);
		PendingIntent pi = PendingIntent.getBroadcast(context, ID_ALARM,
				intent, PendingIntent.FLAG_UPDATE_CURRENT);
		am.setRepeating(AlarmManager.RTC_WAKEUP,
				System.currentTimeMillis() + 3000, period, pi);
	}

	public void CancelAlarm(Context context) {
		Intent intent = new Intent(context, AlarmSmsReceiver.class);
		PendingIntent sender = PendingIntent.getBroadcast(context, ID_ALARM,
				intent, PendingIntent.FLAG_UPDATE_CURRENT);
		AlarmManager alarmManager = (AlarmManager) context
				.getSystemService(Context.ALARM_SERVICE);
		alarmManager.cancel(sender);
	}

	public void setOnetimeTimer(Context context) {
		AlarmManager am = (AlarmManager) context
				.getSystemService(Context.ALARM_SERVICE);
		Intent intent = new Intent(context, AlarmSmsReceiver.class);
		PendingIntent pi = PendingIntent.getBroadcast(context, ID_ALARM,
				intent, PendingIntent.FLAG_UPDATE_CURRENT);
		am.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 30000, pi);
	}

}
