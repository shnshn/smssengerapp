package by.smsender.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import by.smsender.MainActivity;
import by.smsender.R;

public class SetupBroadCast extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		SharedPreferences prefs = context.getSharedPreferences("ShortCutPrefs",
				context.MODE_PRIVATE);
		Log.d("SETUP", "SETUP");
		if (!prefs.getBoolean("isFirstTime", false)) {
			addShortcut(context);
			SharedPreferences.Editor editor = prefs.edit();
			editor.putBoolean("isFirstTime", true);
			editor.commit();
		}

	}

	private void addShortcut(Context context) {
		// Adding shortcut for MainActivity on Home screen
		Intent shortcutIntent = new Intent(context.getApplicationContext(),
				MainActivity.class);

		shortcutIntent.setAction(Intent.ACTION_MAIN);

		Intent addIntent = new Intent();
		addIntent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
		addIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, context.getResources()
				.getString(R.string.app_name));
		addIntent
				.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE,
						Intent.ShortcutIconResource.fromContext(
								context.getApplicationContext(),
								R.drawable.ic_launcher));

		addIntent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
		context.getApplicationContext().sendBroadcast(addIntent);
	}

}
