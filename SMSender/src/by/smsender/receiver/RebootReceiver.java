package by.smsender.receiver;

import by.smsender.helper.PreferenceUtils;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class RebootReceiver extends BroadcastReceiver {
	@Override
	public void onReceive(Context context, Intent intent) {
		if ("android.intent.action.BOOT_COMPLETED".equals(intent.getAction())) {
			Log.d("ALARM_AFTER_REBOOT", " Я вернулся!!!!");
			if (PreferenceUtils.isActiveAlarm(context)) {
				AlarmSmsReceiver alarm = new AlarmSmsReceiver();
				alarm.SetAlarm(context, 60000);
				PreferenceUtils.setStatusAlarm(context, true);
			}
		}
	}
}