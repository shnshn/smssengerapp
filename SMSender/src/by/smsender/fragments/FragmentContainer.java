package by.smsender.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import by.smsender.MainActivity;
import by.smsender.ParentActivity;
import by.smsender.R;
import by.smsender.RegisterActivity;
import by.smsender.views.TitleBarView;

public class FragmentContainer extends ParentFragment {

	@Override
	public void onStart() {
		super.onStart();
	}

	@Override
	public String toString() {
		return "empty";
	}

	@Override
	public void onResume() {
		super.onResume();
		updateBackButton();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_empty, container, false);

		TitleBarView titleBar = ((ParentActivity) getActivity()).getTitleBar();
		titleBar.setSearchButtonHidden(true);
		titleBar.setBackButtonHidden(true);
		titleBar.getBackButton().setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				popChildFragment();
			}
		});

		if (getActivity() != null) {
			if (getActivity() instanceof MainActivity) {

			} else if (getActivity() instanceof RegisterActivity) {
				pushChildFragment(R.id.container, new FragmentLogin());
			}
		}

		return v;
	}

}
