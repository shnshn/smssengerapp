package by.smsender.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import by.smsender.MainActivity;
import by.smsender.R;
import by.smsender.db.DatabaseMgr;
import by.smsender.helper.PreferenceUtils;
import by.smsender.model.RegisttLoginModel;
import by.smsender.model.json.JSONLogin;
import by.smsender.web.SMSHttpClient;
import by.smsender.web.WebUtils;

public class FragmentLogin extends Fragment implements OnClickListener {
	EditText edtEmail;
	EditText edtPassword;
	View progress;
	Button butLogIn;
	Button butRegister;

	@Override
	public void onResume() {
		super.onResume();
		((ParentFragment) getParentFragment()).updateBackButton();

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_login, container, false);
		edtEmail = (EditText) v.findViewById(R.id.edt_login_email);
		edtPassword = (EditText) v.findViewById(R.id.edt_login_password);
		edtEmail.setInputType(InputType.TYPE_CLASS_TEXT
				| InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
		butLogIn = (Button) v.findViewById(R.id.but_login_okey);
		butRegister = (Button) v.findViewById(R.id.but_login_registration);
		butRegister.setOnClickListener(this);
		butLogIn.setOnClickListener(this);
		v.findViewById(R.id.but_about).setOnClickListener(this);
		progress = (View) v.findViewById(R.id.progress_login);

		((TextView) v.findViewById(R.id.txt_forgot_pass))
				.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);

		((TextView) v.findViewById(R.id.txt_forgot_pass))
				.setOnClickListener(this);

		((CheckBox) v.findViewById(R.id.chek_password))
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {
					@Override
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						if (isChecked) {
							edtPassword
									.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
						} else {
							edtPassword.setInputType(129);
						}
						edtPassword
								.setSelection(edtPassword.getText().length());
					}
				});

		return v;
	}

	public void setProgressVisible(boolean flag) {
		if (flag) {
			progress.setVisibility(View.VISIBLE);
			butLogIn.setVisibility(View.GONE);
			butRegister.setVisibility(View.GONE);
		} else {
			progress.setVisibility(View.GONE);
			butLogIn.setVisibility(View.VISIBLE);
			butRegister.setVisibility(View.VISIBLE);
		}
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.but_login_registration:
			((ParentFragment) getParentFragment()).pushChildFragment(
					R.id.container, new FragmentRegistration());

			break;

		case R.id.but_about:
			String url = PreferenceUtils.getCurrentURL(getActivity()).replace(
					"api", "")
					+ "support/how_does_it_work";
			Intent i = new Intent(Intent.ACTION_VIEW);
			i.setData(Uri.parse(url));
			startActivity(i);

			break;

		case R.id.txt_forgot_pass:
			String url2 = PreferenceUtils.getCurrentURL(getActivity()).replace(
					"api", "")
					+ "user/forgot_password";
			Intent i2 = new Intent(Intent.ACTION_VIEW);
			i2.setData(Uri.parse(url2));
			startActivity(i2);

			break;
		case R.id.but_login_okey:

			RegisttLoginModel model = new RegisttLoginModel();
			model.userEmail = edtEmail.getText().toString();
			model.password = edtPassword.getText().toString();
			if (WebUtils.isOnline(getActivity())) {
				AsyncLogin async = new AsyncLogin(model, getActivity()
						.getApplicationContext());
				async.execute();
			} else {
				Toast.makeText(getActivity(), "No internet connection",
						Toast.LENGTH_SHORT).show();
				DatabaseMgr.getInstance(getActivity()).putLog(
						"ERROR. No internet connection for login");
			}

			break;

		default:
			break;
		}

	}

	public class AsyncLogin extends AsyncTask<Void, Void, JSONLogin> {

		RegisttLoginModel model;
		Context context;

		public AsyncLogin(RegisttLoginModel model, Context context) {
			this.model = model;
			this.context = context;
		}

		@Override
		protected JSONLogin doInBackground(Void... params) {

			SMSHttpClient httpClient = new SMSHttpClient(getActivity());
			DatabaseMgr.getInstance(context).putLog(
					"LOGIN -- Email: " + model.userEmail + ".");
			return httpClient.loginUser(model);
		}

		@Override
		protected void onPreExecute() {
			setProgressVisible(true);
			super.onPreExecute();
		}

		@Override
		protected void onPostExecute(JSONLogin result) {
			super.onPostExecute(result);

			setProgressVisible(false);

			if (result == null) {
				Toast.makeText(getActivity(), "Error_server",
						Toast.LENGTH_SHORT).show();
				return;
			}

			DatabaseMgr.getInstance(context).putLog(
					"LOGIN_RESULT (" + model.userEmail + "): "
							+ ((result.res) ? "succes" : result.msg));
			if (!result.res) {

				Toast.makeText(getActivity(), result.msg, Toast.LENGTH_SHORT)
						.show();
			} else {
				PreferenceUtils.setCurrentUserEmail(getActivity(), edtEmail
						.getText().toString());
				PreferenceUtils.setCurrentUserPassword(getActivity(),
						edtPassword.getText().toString());
				PreferenceUtils
						.setCurrentUserToken(getActivity(), result.token);
				if (result.plan >= 1) {
					PreferenceUtils.setUserGoldStatus(getActivity(), true);
				} else {
					PreferenceUtils.setUserGoldStatus(getActivity(), false);
				}
				Intent intent = new Intent(getActivity(), MainActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
				startActivity(intent);
				getActivity().finish();
			}

		}

	}

}
