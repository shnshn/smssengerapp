package by.smsender.fragments;

import java.util.ArrayList;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import by.smsender.ParentActivity;

public class ParentFragment extends Fragment {

	public void updateBackButton() {
		// TODO: refactor this code
		ArrayList<Fragment> fragments = (ArrayList<Fragment>) getChildFragmentManager()
				.getFragments();
		if (fragments != null) {
			int size = fragments.size();
			int backStackEntryCount = getChildFragmentManager()
					.getBackStackEntryCount();
			size = Math.min(size, backStackEntryCount);
			if (size - 1 > 0) {

				Fragment f = fragments.get(size - 1);
				if (f != null) {
					((ParentActivity) getActivity()).getTitleBar()
							.setBackButtonHidden(size == 0);
					((ParentActivity) getActivity()).getTitleBar()
							.setVisibility(View.VISIBLE);
				}
			} else {
				((ParentActivity) getActivity()).getTitleBar()
						.setBackButtonHidden(true);
				((ParentActivity) getActivity()).getTitleBar().setVisibility(
						View.GONE);
			}
		} else if (!((ParentActivity) getActivity()).activity_has_previous) {
			((ParentActivity) getActivity()).getTitleBar().setVisibility(
					View.GONE);

		} else if (((ParentActivity) getActivity()).activity_has_previous) {
			((ParentActivity) getActivity()).getTitleBar().setBackButtonHidden(
					true);

		}

	}

	public void pushChildFragment(int containerId, Fragment f) {
		FragmentTransaction ft = getChildFragmentManager().beginTransaction();

		String simpleName = ((Object) f).getClass().getSimpleName();
		ft.replace(containerId, f, simpleName);
		ft.addToBackStack(simpleName);
		ft.commit();

		((ParentActivity) getActivity()).getTitleBar().setBackButtonHidden(
				false);

		ArrayList<Fragment> fragments = (ArrayList<Fragment>) getChildFragmentManager()
				.getFragments();
		if (fragments != null) {
			int size = fragments.size();

			if (size > 0) {
				((ParentActivity) getActivity()).getTitleBar().setVisibility(
						View.VISIBLE);
			}
		} else {
			((ParentActivity) getActivity()).getTitleBar().setVisibility(
					View.GONE);
		}

	}

	public void popChildFragment() {
		// TODO: refactor this code
		ArrayList<Fragment> fragments = (ArrayList<Fragment>) getChildFragmentManager()
				.getFragments();
		if (fragments != null) {
			int size = fragments.size();
			int backStackEntryCount = getChildFragmentManager()
					.getBackStackEntryCount();
			size = Math.min(size, backStackEntryCount);
			if (size - 1 > 0) {
				Fragment f = fragments.get(size - 1);
				if (f != null) {
					FragmentTransaction ft = getChildFragmentManager()
							.beginTransaction();
					ft.remove(f);
					ft.commit();
					getChildFragmentManager().popBackStack();
					size--;
					((ParentActivity) getActivity()).getTitleBar()
							.setBackButtonHidden(size == 0 || size == 1);
				}
			}
		}
	}

	public void popToRoot() {
		ArrayList<Fragment> fragments = (ArrayList<Fragment>) getChildFragmentManager()
				.getFragments();
		if (fragments != null) {
			int count = fragments.size();
			for (int i = count - 1; i >= 0; i--) {
				Fragment f = fragments.get(i);
				if (f != null) {
					FragmentTransaction ft = getChildFragmentManager()
							.beginTransaction();
					ft.remove(f);
					ft.commit();
					getChildFragmentManager().popBackStack();
				}
			}
			((ParentActivity) getActivity()).getTitleBar().setBackButtonHidden(
					true);
			onPopToRootCompleted();
		}
	}

	protected void onPopToRootCompleted() {
	}

}
