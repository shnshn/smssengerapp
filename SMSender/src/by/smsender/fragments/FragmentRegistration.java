package by.smsender.fragments;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import by.smsender.ParentActivity;
import by.smsender.R;
import by.smsender.db.DatabaseMgr;
import by.smsender.model.RegisttLoginModel;
import by.smsender.model.json.JSONRegistration;
import by.smsender.web.SMSHttpClient;
import by.smsender.web.WebUtils;

public class FragmentRegistration extends Fragment implements OnClickListener {

	EditText edtEmail;
	EditText edtPassword;
	View progress;
	Button butRegister;

	@Override
	public void onResume() {
		super.onResume();
		((ParentActivity) getActivity()).getTitleBar().setTitle("Registration");

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_registration, container,
				false);

		butRegister = (Button) v.findViewById(R.id.but_registrate_okey);
		butRegister.setOnClickListener(this);
		edtEmail = (EditText) v.findViewById(R.id.edt_registr_email);
		edtEmail.setInputType(InputType.TYPE_CLASS_TEXT
				| InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
		edtPassword = (EditText) v.findViewById(R.id.edt_registr_password);
		progress = (View) v.findViewById(R.id.progress_login);

		return v;
	}

	public void setProgressVisible(boolean flag) {
		if (flag) {
			progress.setVisibility(View.VISIBLE);
			butRegister.setVisibility(View.GONE);
		} else {
			progress.setVisibility(View.GONE);
			butRegister.setVisibility(View.VISIBLE);
		}
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.but_registrate_okey:
			RegisttLoginModel model = new RegisttLoginModel();
			model.userEmail = edtEmail.getText().toString();
			model.password = edtPassword.getText().toString();
			if (WebUtils.isOnline(getActivity())) {
				AsyncRegistrate async = new AsyncRegistrate(model,
						getActivity().getApplicationContext());

				async.execute();
			} else {
				Toast.makeText(getActivity(), "No internet connection",
						Toast.LENGTH_SHORT).show();
				DatabaseMgr.getInstance(getActivity()).putLog("ERROR. No internet connection for registration");
			}
			break;

		default:
			break;
		}

	}

	public class AsyncRegistrate extends
			AsyncTask<Void, Void, JSONRegistration> {

		RegisttLoginModel model;
		Context context;

		public AsyncRegistrate(RegisttLoginModel model, Context context) {
			this.model = model;
			this.context = context;
		}

		@Override
		protected JSONRegistration doInBackground(Void... params) {

			SMSHttpClient httpClient = new SMSHttpClient(getActivity());
			DatabaseMgr.getInstance(context).putLog(
					"REGISTRATION -- Email: " + model.userEmail+".");
			return httpClient.registrateUser(model);
		}

		@Override
		protected void onPreExecute() {
			setProgressVisible(true);
			super.onPreExecute();
		}

		@Override
		protected void onPostExecute(JSONRegistration result) {
			super.onPostExecute(result);
			setProgressVisible(false);

			if (result == null) {
				Toast.makeText(getActivity(), "Error_server",
						Toast.LENGTH_SHORT).show();
				return;
			}

			String predicate = "";
			if (result.res) {
				predicate = "Succes: ";
			} else {
				predicate = "Fail: ";
			}

			DatabaseMgr.getInstance(context).putLog(
					"REGISTRATION_RESULT(" + model.userEmail + "): "
							+ predicate + result.msg);

			Toast.makeText(getActivity(), predicate + result.msg,
					Toast.LENGTH_LONG).show();
			if (result.res) {
				((ParentFragment) getParentFragment()).popChildFragment();
			}

		}

	}

}
