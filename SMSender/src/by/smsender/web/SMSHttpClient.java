package by.smsender.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import by.smsender.db.DatabaseMgr;
import by.smsender.helper.JsonDinamucallyKeysHelper;
import by.smsender.helper.PreferenceUtils;
import by.smsender.model.Constants;
import by.smsender.model.RegisttLoginModel;
import by.smsender.model.SMSTask;
import by.smsender.model.json.JSONLogin;
import by.smsender.model.json.JSONRegistration;
import by.smsender.model.json.JSONTask;

import com.google.gson.Gson;

public class SMSHttpClient extends DefaultHttpClient {
	Context context;

	public SMSHttpClient(Context context) {
		this.context = context;
	}

	protected String postHttpData(String url, List<NameValuePair> nameValuePairs) {
		// Create a new HttpClient and Post Header
		String result = "";
		HttpPost httppost = new HttpPost(url);

		try {
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			HttpResponse response = execute(httppost);
			result = EntityUtils.toString(response.getEntity());
			return result;
		} catch (ClientProtocolException e) {
			DatabaseMgr.getInstance(context).putLog("ERROR in the sending of the server. (URL IS WRONG???)");
		} catch (IOException e) {
			DatabaseMgr.getInstance(context).putLog("ERROR in the sending of the server.");
		}

		return null;
	}

	protected String getHttpData(String url) {
		// Create a new HttpClient and Post Header
		String result = "";
		HttpGet httpget = new HttpGet(url);

		try {
			HttpResponse response = execute(httpget);
			result = EntityUtils.toString(response.getEntity());
			return result;
		} catch (ClientProtocolException e) {
			DatabaseMgr.getInstance(context).putLog("ERROR get data of the server. (URL IS WRONG???)");
		} catch (IOException e) {
			DatabaseMgr.getInstance(context).putLog("ERROR get data of the server.");
		}

		return null;
	}

	protected String generateGetURL(String url, List<NameValuePair> params) {
		if (!url.endsWith("?"))
			url += "?";
		String paramString = URLEncodedUtils.format(params, "utf-8");
		url += paramString;
		return url;
	}

	public JSONRegistration registrateUser(RegisttLoginModel model) {
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add(new BasicNameValuePair(Constants.KEY_EMAIL,
				model.userEmail));
		nameValuePairs.add(new BasicNameValuePair(Constants.KEY_PASSWORD,
				model.password));

		String result = getHttpData(generateGetURL(
				PreferenceUtils.getCurrentURL(context)
						+ Constants.URL_REGISTRATION, nameValuePairs));

		if (result == null || result.isEmpty())
			return null;
		Gson gson = new Gson();
		JSONRegistration obj = gson.fromJson(result, JSONRegistration.class);

		return obj;
	}

	public JSONLogin loginUser(RegisttLoginModel model) {
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add(new BasicNameValuePair(Constants.KEY_EMAIL,
				model.userEmail));
		nameValuePairs.add(new BasicNameValuePair(Constants.KEY_PASSWORD,
				model.password));

		String result = getHttpData(generateGetURL(
				PreferenceUtils.getCurrentURL(context) + Constants.URL_LOGIN,
				nameValuePairs));

		if (result == null || result.isEmpty())
			return null;
		Log.d("NEW_API", "LOGIN -- " + result);
		Gson gson = new Gson();
		JSONLogin obj = gson.fromJson(result, JSONLogin.class);

		return obj;
	}

	public JSONTask getTasks(String email, String token, String id) {
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add(new BasicNameValuePair(Constants.KEY_EMAIL, email));
		nameValuePairs.add(new BasicNameValuePair(Constants.KEY_TOKEN, token));
		nameValuePairs.add(new BasicNameValuePair(Constants.KEY_ID, id));

		String result = getHttpData(generateGetURL(
				PreferenceUtils.getCurrentURL(context)
						+ Constants.URL_GET_TASKS, nameValuePairs));
		

		if (result == null || result.isEmpty())
			return null;
		Gson gson = new Gson();
		JSONTask obj = gson.fromJson(result, JSONTask.class);

		// try {
		// JSONObject allJson = new JSONObject(result);
		// JSONObject numbers = allJson.getJSONObject("nums");
		// List keys = null;
		// try {
		// keys = JsonDinamucallyKeysHelper.getKeysFromJson(numbers
		// .toString());
		// } catch (Exception e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// if (keys != null) {
		// obj.nums = new ArrayList<SMSTask>();
		// for (Object item : keys) {
		// SMSTask task = new SMSTask();
		// task.num = (String) item;
		// task.msg = (String) numbers.getString(task.num);
		// obj.nums.add(task);
		//
		// }
		// }
		//
		// } catch (JSONException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }

		return obj;
	}

	public void updateTaskStatus(String email, String token, List<SMSTask> tasks) {
		List<NameValuePair> nameValuePairs_url = new ArrayList<NameValuePair>(2);
		nameValuePairs_url.add(new BasicNameValuePair(Constants.KEY_EMAIL,
				email));
		nameValuePairs_url.add(new BasicNameValuePair(Constants.KEY_TOKEN,
				token));

		List<NameValuePair> nameValuePairs_post = new ArrayList<NameValuePair>();
		nameValuePairs_post.add(new BasicNameValuePair("nums",
				JsonDinamucallyKeysHelper.createStringJsonArray(tasks)));
		Log.d("SMS_STATUS",
				JsonDinamucallyKeysHelper.createStringJsonArray(tasks));

		String result = postHttpData(
				generateGetURL(PreferenceUtils.getCurrentURL(context)
						+ Constants.URL_UPDATE_STATUS, nameValuePairs_url),
				nameValuePairs_post);

		Log.d("SMS_STATUS", result);

	}

}
