package by.smsender.web;

public enum PostAction {

	getComplex("get_complex"), getDepartment("get_department"), getService(
			"get_service"), getDisease("get_disease"), getPage("get_page"), getRegistration(
			"make_appointment"), getContact("get_contact");

	private PostAction(String actionName) {
		this.actionName = actionName;
	}

	public String getActionName() {

		return actionName;
	}

	String actionName;
}
