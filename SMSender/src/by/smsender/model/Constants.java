package by.smsender.model;

public class Constants {
	public static String URL_DEFAULT = "http://www.smssenger.com/api";
	public static String URL_REGISTRATION = "/registration";
	public static String URL_LOGIN = "/login";
	public static String URL_GET_TASKS = "/task";
	public static String URL_UPDATE_STATUS = "/taskStatus";

	public static String KEY_EMAIL = "email";
	public static String KEY_PASSWORD = "passwd";
	public static String KEY_TOKEN = "token";
	public static String KEY_ID = "id";

}
