package by.smsender;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import by.smsender.views.TitleBarView;

public class ParentActivity extends FragmentActivity {
	protected TitleBarView mTitleBar;
	public boolean activity_has_previous = false;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// getActionBar().hide();
		super.onCreate(savedInstanceState);
		// getSupportActionBar().setIcon(
		// new ColorDrawable(getResources().getColor(
		// android.R.color.transparent)));
		// getSupportActionBar().setBackgroundDrawable(
		// new ColorDrawable(Color.parseColor("#6F9F4D")));

	}

	public TitleBarView getTitleBar() {
		return mTitleBar;
	}

	private Fragment getHomeFragment() {
		return getSupportFragmentManager().findFragmentByTag("tab0");
	}

	public void replaceFragment(Fragment fragment, boolean flag_back) {

		FragmentManager fragmentManager = getSupportFragmentManager();

		FragmentTransaction fragmentTransaction = fragmentManager
				.beginTransaction();
		fragmentTransaction.replace(R.id.container_fragment, fragment,
				fragment.toString());
		if (flag_back)
			fragmentTransaction.addToBackStack(fragment.toString());
		fragmentTransaction.commit();
	}

}
