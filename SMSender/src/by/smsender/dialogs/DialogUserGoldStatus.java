package by.smsender.dialogs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import by.smsender.R;
import by.smsender.helper.PreferenceUtils;

public class DialogUserGoldStatus extends DialogFragment implements
		OnClickListener {

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
		View v = inflater.inflate(R.layout.dialog_user_gold, null);

		v.findViewById(R.id.but_ok).setOnClickListener(this);

		return v;
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.but_ok:

			dismiss();

			break;

		default:
			break;
		}

	}

}
