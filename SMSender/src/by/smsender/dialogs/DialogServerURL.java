package by.smsender.dialogs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.Toast;
import by.smsender.R;
import by.smsender.helper.PreferenceUtils;

public class DialogServerURL extends DialogFragment implements OnClickListener {

	EditText edtSever;

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
		View v = inflater.inflate(R.layout.dialog_server_url, null);
		edtSever = (EditText) v.findViewById(R.id.edt_server_url);
		edtSever.setText(PreferenceUtils.getCurrentURL(getActivity()));
		v.findViewById(R.id.but_ok).setOnClickListener(this);
		v.findViewById(R.id.but_cancel).setOnClickListener(this);

		return v;
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.but_ok:
			if (edtSever == null)
				return;
			String url_from_edt = edtSever.getText().toString();
			if (url_from_edt.equals(PreferenceUtils.getCurrentURL(getActivity()
					.getApplicationContext()))) {
				Toast.makeText(getActivity().getApplicationContext(),
						"Value is not changed", Toast.LENGTH_SHORT).show();
			} else {
				PreferenceUtils.setCurrentUrl(getActivity()
						.getApplicationContext(), url_from_edt);
			}
			dismiss();

			break;
		case R.id.but_cancel:
			dismiss();

			break;

		default:
			break;
		}

	}

}
