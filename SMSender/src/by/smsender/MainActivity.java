package by.smsender;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.ToggleButton;
import by.smsender.asyncTasks.AsynkUpdateTaskStatus;
import by.smsender.db.DatabaseMgr;
import by.smsender.fragments.ParentFragment;
import by.smsender.helper.ConstantsPreferenceKeys;
import by.smsender.helper.PreferenceUtils;
import by.smsender.receiver.AlarmSmsReceiver;
import by.smsender.views.TitleBarView;
import by.smsender.web.WebUtils;

@SuppressLint("NewApi")
public class MainActivity extends ParentActivity implements OnClickListener {
	AlarmSmsReceiver alarm;

	// EditText edtNumber;
	// EditText edtText;
	TextView txtStatus;
	StatusActivityReceiver statusActivityReceiver;
	TextView txtInterval;

	@Override
	protected void onResume() {
		super.onResume();
		IntentFilter filter = new IntentFilter(getResources().getString(
				R.string.activity_status_action));
		filter.addCategory(Intent.CATEGORY_DEFAULT);
		statusActivityReceiver = new StatusActivityReceiver();
		registerReceiver(statusActivityReceiver, filter);
		if (txtInterval != null) {
			setTitleInterval(PreferenceUtils.getInt(getApplicationContext(),
					ConstantsPreferenceKeys.KEY_CURRENT_HOUR),
					PreferenceUtils.getInt(getApplicationContext(),
							ConstantsPreferenceKeys.KEY_CURRENT_MINUTE),
					PreferenceUtils.getInt(getApplicationContext(),
							ConstantsPreferenceKeys.KEY_CURRENT_SECONDS));
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		unregisterReceiver(statusActivityReceiver);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		alarm = new AlarmSmsReceiver();
		// edtNumber = (EditText) findViewById(R.id.edt_sms_number);
		// edtText = (EditText) findViewById(R.id.edt_sms_text);
		mTitleBar = (TitleBarView) findViewById(R.id.top_title_bar);
		mTitleBar.hideAllBackButton();
		mTitleBar.setTitle("SMSSenger");

		mTitleBar.getSearchButton().setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(),
						SettingsActivity.class);
				startActivity(intent);

			}
		});

		txtStatus = (TextView) findViewById(R.id.txt_status);
		txtInterval = (TextView) findViewById(R.id.txt_interval_title);

		final ToggleButton toggleButton = (ToggleButton) findViewById(R.id.togl_but_start_service);
		toggleButton.setChecked(PreferenceUtils.isActiveAlarm(this));

		toggleButton.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				PreferenceUtils.setStatusAlarm(MainActivity.this, isChecked);
				cansenAlarm();
				if (isChecked) {

					startAlarm(PreferenceUtils
							.getInterval(getApplicationContext()));
				} else {
					if (WebUtils.isOnline(MainActivity.this)) {

						if (PreferenceUtils.isOnlyWifi(MainActivity.this)
								&& WebUtils.isWifi(MainActivity.this)) {
							AsynkUpdateTaskStatus syStatus = new AsynkUpdateTaskStatus(
									getApplicationContext());
							syStatus.execute();
						} else if (!PreferenceUtils
								.isOnlyWifi(MainActivity.this)) {
							AsynkUpdateTaskStatus syStatus = new AsynkUpdateTaskStatus(
									getApplicationContext());
							syStatus.execute();
						}

					} else {
						DatabaseMgr
								.getInstance(MainActivity.this)
								.putLog("ERROR. No internet connection for send status task");
					}
				}

				setStatusService(PreferenceUtils
						.isActiveAlarm(getApplicationContext()));

			}
		});

		setTitleInterval(PreferenceUtils.getInt(getApplicationContext(),
				ConstantsPreferenceKeys.KEY_CURRENT_HOUR),
				PreferenceUtils.getInt(getApplicationContext(),
						ConstantsPreferenceKeys.KEY_CURRENT_MINUTE),
				PreferenceUtils.getInt(getApplicationContext(),
						ConstantsPreferenceKeys.KEY_CURRENT_SECONDS));

		setStatusService(PreferenceUtils.isActiveAlarm(getApplicationContext()));
		findViewById(R.id.lay_about_but).setOnClickListener(this);

	}

	public void setTitleInterval(int hour, int minute, int sec) {
		String text = "The service will send SMS every ";
		if (hour > 0) {
			text += hour + "h ";
		}
		if (minute > 0) {
			text += minute + "m ";
		}

		text += sec + "s ";

		txtInterval.setText(text);
	}

	public void setStatusService(boolean flag) {
		if (flag) {
			txtStatus.setText(R.string.service_working);
		} else {
			txtStatus.setText(R.string.service_stopped);
		}
	}

	public void startAlarm(long interval) {
		alarm.SetAlarm(getApplicationContext(), interval);
		PreferenceUtils.setStatusAlarm(getApplicationContext(), true);
	}

	public void cansenAlarm() {
		alarm.CancelAlarm(getApplicationContext());
		PreferenceUtils.setStatusAlarm(getApplicationContext(), false);
	}

	@Override
	public void onBackPressed() {

		// int count = getFragmentManager().getBackStackEntryCount();
		ParentFragment pFrag = (ParentFragment) getSupportFragmentManager()
				.findFragmentByTag("empty");
		if (mTitleBar.getBackButton().getVisibility() == View.GONE) {
			super.onBackPressed();
		} else if (mTitleBar.getBackButton().getVisibility() == View.VISIBLE) {
			pFrag.popChildFragment();
		}

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {

		case R.id.lay_about_but:
			String url = PreferenceUtils.getCurrentURL(MainActivity.this)
					.replace("api", "") + "support/how_does_it_work";
			Intent i = new Intent(Intent.ACTION_VIEW);
			i.setData(Uri.parse(url));
			startActivity(i);

			break;

		default:
			break;
		}

	}

	public class StatusActivityReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			if (txtStatus != null) {
				txtStatus.setText("Send sms");
			}

		}

	}

}
