package by.smsender;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import by.smsender.asyncTasks.AsynkUpdateTaskStatus;
import by.smsender.db.DatabaseMgr;
import by.smsender.dialogs.DialogServerURL;
import by.smsender.dialogs.DialogUserGoldStatus;
import by.smsender.fragments.ParentFragment;
import by.smsender.helper.ConstantsPreferenceKeys;
import by.smsender.helper.PreferenceUtils;
import by.smsender.model.Constants;
import by.smsender.receiver.AlarmSmsReceiver;
import by.smsender.views.TitleBarView;
import by.smsender.web.WebUtils;

import com.ikovac.timepickerwithseconds.view.MyTimePickerDialog;
import com.ikovac.timepickerwithseconds.view.MyTimePickerDialog.OnTimeSetListener;
import com.ikovac.timepickerwithseconds.view.TimePicker;

public class SettingsActivity extends ParentActivity implements OnClickListener {

	AlarmSmsReceiver alarm;
	TextView txtUserEmail;
	EditText edtHour;
	EditText edtMinute;
	EditText edtSeconds;
	EditText edtURLServer;
	CheckBox checkWifi;

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.settings_activity);
		alarm = new AlarmSmsReceiver();

		mTitleBar = (TitleBarView) findViewById(R.id.top_title_bar);
		mTitleBar.setBackButtonHidden(true);
		mTitleBar.setSearchButtonHidden(true);
		mTitleBar.setTitle("Settings");
		
		OnClickListener clickListener = new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				onBackPressed();
				
			}
		};
		mTitleBar.getBackActivityButton().setOnClickListener(clickListener);
		mTitleBar.getIcon().setOnClickListener(clickListener);

		mTitleBar.getSearchButton().setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Toast.makeText(getApplicationContext(), "Settings",
						Toast.LENGTH_SHORT).show();

			}
		});

		txtUserEmail = (TextView) findViewById(R.id.txt_value_user_login);
		txtUserEmail.setText(PreferenceUtils
				.getCurrentUserEmail(getApplicationContext()));

		final ToggleButton toggleButton = (ToggleButton) findViewById(R.id.togbut_wifi);
		toggleButton.setChecked(PreferenceUtils
				.isOnlyWifi(getApplicationContext()));

		toggleButton.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {

				// if (!PreferenceUtils.isUserGold(getApplicationContext())) {
				// showDialogNoAllowFeatures();
				// toggleButton.setChecked(false);
				// PreferenceUtils.setWifiFlaf(getApplicationContext(), false);
				// return;
				// }

				PreferenceUtils.setWifiFlaf(getApplicationContext(), isChecked);

			}
		});

		findViewById(R.id.rel_server).setOnClickListener(this);
		findViewById(R.id.rel_timer).setOnClickListener(this);
		findViewById(R.id.rel_user_about).setOnClickListener(this);

		findViewById(R.id.rel_log).setOnClickListener(this);

	}

	public void showDialogNoAllowFeatures() {
		DialogUserGoldStatus dialogUserGoldStatus = new DialogUserGoldStatus();
		dialogUserGoldStatus.show(getSupportFragmentManager(), "");
	}

	public void showTomePickerDialog(int hour, int minute, int seconds) {
		MyTimePickerDialog dialog = new MyTimePickerDialog(this,
				new OnTimeSetListener() {

					@Override
					public void onTimeSet(TimePicker view, int hourOfDay,
							int minute, int seconds) {
						long interval = seconds * 1000 + minute * 60000
								+ hourOfDay * 3600000000l;

						if (interval < 30000) {
							edtSeconds
									.setError("Interval can not be less than 30 seconds!");
							return;
						}

						if (PreferenceUtils
								.getInterval(getApplicationContext()) == interval) {
							Toast.makeText(getApplicationContext(),
									"Value is not changed", Toast.LENGTH_SHORT)
									.show();
						} else {
							Toast.makeText(getApplicationContext(), "Saved!",
									Toast.LENGTH_SHORT).show();
							if (PreferenceUtils
									.isActiveAlarm(getApplicationContext())) {
								cansenAlarm();
								startAlarm(interval);

							}
							PreferenceUtils.setInt(getApplicationContext(),
									ConstantsPreferenceKeys.KEY_CURRENT_HOUR,
									hourOfDay);
							PreferenceUtils.setInt(getApplicationContext(),
									ConstantsPreferenceKeys.KEY_CURRENT_MINUTE,
									minute);
							PreferenceUtils
									.setInt(getApplicationContext(),
											ConstantsPreferenceKeys.KEY_CURRENT_SECONDS,
											seconds);
							PreferenceUtils.setInterval(
									getApplicationContext(), interval);
						}

					}
				}, hour, minute, seconds, true);
		dialog.show();
	}

	public void startAlarm(long interval) {
		alarm.SetAlarm(getApplicationContext(), interval);
		PreferenceUtils.setStatusAlarm(getApplicationContext(), true);
	}

	public void cansenAlarm() {
		alarm.CancelAlarm(getApplicationContext());
		PreferenceUtils.setStatusAlarm(getApplicationContext(), false);
	}

	@Override
	public void onBackPressed() {

		// int count = getFragmentManager().getBackStackEntryCount();
		ParentFragment pFrag = (ParentFragment) getSupportFragmentManager()
				.findFragmentByTag("empty");
		if (mTitleBar.getBackButton().getVisibility() == View.GONE) {
			super.onBackPressed();
		} else if (mTitleBar.getBackButton().getVisibility() == View.VISIBLE) {
			pFrag.popChildFragment();
		}

	}

	public void ClearUserData() {
		PreferenceUtils.setCurrentUserEmail(getApplicationContext(), "");
		PreferenceUtils.setCurrentUserPassword(getApplicationContext(), "");
		PreferenceUtils.setCurrentUserToken(getApplicationContext(), "");
		PreferenceUtils.setInt(getApplicationContext(),
				ConstantsPreferenceKeys.KEY_CURRENT_HOUR, 0);
		PreferenceUtils.setInt(getApplicationContext(),
				ConstantsPreferenceKeys.KEY_CURRENT_MINUTE, 0);
		PreferenceUtils.setInt(getApplicationContext(),
				ConstantsPreferenceKeys.KEY_CURRENT_SECONDS, 30);
		PreferenceUtils.setInterval(getApplicationContext(), 30000);
		PreferenceUtils.setWifiFlaf(getApplicationContext(), false);
		PreferenceUtils.setCurrentUrl(getApplicationContext(),
				Constants.URL_DEFAULT);
		PreferenceUtils.setUserGoldStatus(getApplicationContext(), false);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.rel_user_about:
			if (WebUtils.isOnline(this)) {

				if (PreferenceUtils.isOnlyWifi(this) && WebUtils.isWifi(this)) {
					AsynkUpdateTaskStatus syStatus = new AsynkUpdateTaskStatus(
							getApplicationContext());
					syStatus.execute();
				} else if (!PreferenceUtils.isOnlyWifi(this)) {
					AsynkUpdateTaskStatus syStatus = new AsynkUpdateTaskStatus(
							getApplicationContext());
					syStatus.execute();
				}

			}
			ClearUserData();
			Intent intent = new Intent(this, RegisterActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
					| Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			this.finish();

			break;

		case R.id.rel_server:
			if (!PreferenceUtils.isUserGold(getApplicationContext())) {
				showDialogNoAllowFeatures();
				return;
			}
			DialogServerURL dialogServerURL = new DialogServerURL();
			dialogServerURL.show(getSupportFragmentManager(), "");

			break;
		case R.id.rel_log:
			Intent intent2 = new Intent(this, LogActivity.class);
			startActivity(intent2);

			break;

		case R.id.rel_timer:
			if (!PreferenceUtils.isUserGold(getApplicationContext())) {
				showDialogNoAllowFeatures();
				return;
			}

			int hour = PreferenceUtils.getInt(getApplicationContext(),
					ConstantsPreferenceKeys.KEY_CURRENT_HOUR);
			int minute = PreferenceUtils.getInt(getApplicationContext(),
					ConstantsPreferenceKeys.KEY_CURRENT_MINUTE);
			int sek = PreferenceUtils.getInt(getApplicationContext(),
					ConstantsPreferenceKeys.KEY_CURRENT_SECONDS);
			showTomePickerDialog(hour, minute, sek);

			break;
		default:
			break;
		}

	}
}